#!/usr/bin/python
import boto3
import datetime, json
metrics = []
metric = []
ec2client = boto3.client('ec2')
ec2resource = boto3.resource('ec2')
instances = ec2resource.instances.filter(
    Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])
count = 0
for i in instances:
    if (count == 0):
        metric = ["AWS/EC2","CPUUtilization","InstanceId",i.id]
    else:
        metric = [".",".",".",i.id]
    metrics.append(metric)
    count +=1
#print(metrics)

widget1 = {
         "type":"metric",
         "x":0,
         "y":0,
         "width":12,
         "height":6,
         "properties":{
            "view": "timeSeries",
            "metrics": metrics,
            "period": 300,
            "stat":"Average",
            "region":"ap-southeast-1",
            "title":"EC2 Instance CPU Utilization"
         }
      }
#print(widget1)
dashboard_body = { "widgets": [ widget1 ] }
dash_body = json.dumps(dashboard_body)
print(dash_body)

#asg_name = "Prod-digipace-analytics-ASG"
cwclient = boto3.client('cloudwatch')
#response = client.get_dashboard(
#    DashboardName='Digipace-Monitoring-Dashboard'
#)
#old_dash = response['DashboardBody']

response = cwclient.put_dashboard(
    DashboardName='All-Instance-CPU-Util',
    DashboardBody=dash_body
)
print(response)
exit()

# response = client.describe_auto_scaling_groups()
response = client.describe_auto_scaling_groups(AutoScalingGroupNames=[asg_name])
all_asg = response['AutoScalingGroups']
print(all_asg)
print("ASG Name: {0}".format(colored(all_asg[0]['AutoScalingGroupName'], 'cyan')))
for i in range(len(all_asg)):
    print("ASG Name: {0}".format(colored(all_asg[i]['AutoScalingGroupName'], 'cyan')))
    print("Desired Capacity: {0}".format(colored(all_asg[i]['DesiredCapacity'], 'cyan')))
    print("Minimum Size: {0}".format(colored(all_asg[i]['MinSize'], 'cyan')))
    print("Maximum Size: {0}".format(colored(all_asg[i]['MaxSize'], 'cyan')))
    print("Default Cooldown: {0}".format(colored(all_asg[i]['DefaultCooldown'], 'cyan')))
    print("Created Time: {0}".format(colored(all_asg[i]['CreatedTime'], 'cyan')))
    print("Launch Configuration Name: {0}".format(colored(all_asg[i]['LaunchConfigurationName'], 'cyan')))
    all_lbs = all_asg[i]['LoadBalancerNames']
    for l in range(len(all_lbs)):
        print("Load Balancer: {0}".format(colored(all_lbs[l], 'cyan')))
    all_ec2s = all_asg[i]['Instances']
    print("Instance Details")
    for m in range(len(all_ec2s)):
        print("- Instance Id: {0}".format(colored(all_ec2s[m]['InstanceId'], 'cyan')))
        print("- Protected From Scale In: {0}".format(colored(all_ec2s[m]['ProtectedFromScaleIn'], 'cyan')))
        print("- Launch Configuration Name: {0}".format(colored(all_ec2s[m]['LaunchConfigurationName'], 'cyan')))
