import boto3

client = boto3.client('autoscaling')

response = client.create_launch_configuration(
    LaunchConfigurationName='TestLC1',
    ImageId='ami-7d89b501',
    KeyName='WD-Key',
    SecurityGroups=[
        'sg-9aa406fc',
    ],
    InstanceType='t2.small',
    BlockDeviceMappings=[
        {
            'VirtualName': '/dev/xvda',
            'DeviceName': '/dev/xvda',
            'Ebs': {
                'SnapshotId': 'snap-048cbb91c10986e8a',
                'VolumeSize': 8,
                'VolumeType': 'gp2',
                'DeleteOnTermination': True,
                'Iops': 123,
                'Encrypted': False
            },
            'NoDevice': True|False
        },
    ],
    InstanceMonitoring={
        'Enabled': False
    },
    EbsOptimized=False,
    AssociatePublicIpAddress=True
)
