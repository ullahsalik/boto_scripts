#!/usr/bin/python
import boto3
import datetime
from termcolor import colored

asg_name = " prod-node-js-analytics-27-july-2018-ASG"

client = boto3.client('autoscaling')
# response = client.describe_auto_scaling_groups()
response = client.describe_auto_scaling_groups(AutoScalingGroupNames=[asg_name])
all_asg = response['AutoScalingGroups']
print(all_asg)
print("ASG Name: {0}".format(colored(all_asg[0]['AutoScalingGroupName'], 'cyan')))
for i in range(len(all_asg)):
    print("ASG Name: {0}".format(colored(all_asg[i]['AutoScalingGroupName'], 'cyan')))
    print("Desired Capacity: {0}".format(colored(all_asg[i]['DesiredCapacity'], 'cyan')))
    print("Minimum Size: {0}".format(colored(all_asg[i]['MinSize'], 'cyan')))
    print("Maximum Size: {0}".format(colored(all_asg[i]['MaxSize'], 'cyan')))
    print("Default Cooldown: {0}".format(colored(all_asg[i]['DefaultCooldown'], 'cyan')))
    print("Created Time: {0}".format(colored(all_asg[i]['CreatedTime'], 'cyan')))
    print("Launch Configuration Name: {0}".format(colored(all_asg[i]['LaunchConfigurationName'], 'cyan')))
    all_lbs = all_asg[i]['LoadBalancerNames']
    for l in range(len(all_lbs)):
        print("Load Balancer: {0}".format(colored(all_lbs[l], 'cyan')))
    all_ec2s = all_asg[i]['Instances']
    print("Instance Details")
    for m in range(len(all_ec2s)):
        print("- Instance Id: {0}".format(colored(all_ec2s[m]['InstanceId'], 'cyan')))
        print("- Protected From Scale In: {0}".format(colored(all_ec2s[m]['ProtectedFromScaleIn'], 'cyan')))
        print("- Launch Configuration Name: {0}".format(colored(all_ec2s[m]['LaunchConfigurationName'], 'cyan')))
