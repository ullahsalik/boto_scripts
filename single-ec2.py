#!/usr/bin/python3
import boto3
import collections
import datetime
import time

ec2client = boto3.client('ec2')
ec2resource = boto3.resource('ec2')

instances = ec2resource.instances.filter(
        Filters=[{'Name': 'instance-state-name', 'Values': ['running']},
            {'Name': 'ip-address', 'Values':["52.220.129.15"]}])
for instance in instances:
    print(instance)
