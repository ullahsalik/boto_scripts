#!/usr/bin/python
import boto3
client = boto3.client('dynamodb')

response = client.get_item(
   TableName = "aws_sample_test",
   Key = {
       "title":"munich"
   })
print(response)
exit()
response = client.update_item(
   TableName = "aws_sample_test",
   Key = {
       "title":"munich"
   },
   UpdateExpression = "SET director = :label",
   ExpressionAttributeValues = { 
       ":label": "Ankush"
   })
print(response)
